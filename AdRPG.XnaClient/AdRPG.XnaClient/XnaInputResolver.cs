﻿using System.Collections.Generic;
using System.Linq;
using AdRPG.Logic.World.Loc;
using AdRPG.XnaClient.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Game = AdRPG.Logic.Game.Game;

namespace AdRPG.XnaClient
{
    enum EnumAction
    {
        GoUp,
        GoDown,
        GoLeft,
        GoRight,
        CommandInputOn,
        Interact,
        Wait,
        PauseMenu
    }


    public class XnaInputResolver
    {
        private static readonly IDictionary<EnumAction, Keys[]> KeyMappings = new Dictionary<EnumAction, Keys[]>
        {
            //Defaults
            { EnumAction.GoUp, new[] {Keys.W }},
            { EnumAction.GoLeft, new[] {Keys.A }},
            { EnumAction.GoDown, new[] {Keys.S }},
            { EnumAction.GoRight, new[] { Keys.D }},
            { EnumAction.CommandInputOn, new [] { Keys.Enter }},
            { EnumAction.Interact, new [] { Keys.T }},
            { EnumAction.Wait, new [] { Keys.V }},
            { EnumAction.PauseMenu, new[] { Keys.Escape }}
        };

        public static bool ResolvePlayerInput(GameTime gameTime)
        {
            var keys = KeyboardInputHelper.CurrentState.GetPressedKeys();
            if (!keys.Any()) { return false; }

            if (XnaUI.CurrentUIMenu != null)
            {
                var result = XnaUI.CurrentUIMenu.ResolveInput();

                if (result.ExitMenu)
                {
                    XnaUI.CurrentUIMenu = null;
                    return result.NeedUpdate;
                }

                return false;
            }

            if (!ResolveUIInteractions()) return false;

            var movingTo = DetectDirectionKey();
            if (movingTo != EnumDirection.Nowhere)
            {
                var result = Game.Instance.CurrentPlayer.Hero.TryMove(movingTo);
                result.Messages.ForEach(m => XnaUI.Console.AddMessage(m));
                return result.Success;
            }

            if (DetectActionWithoutBuffering(EnumAction.Wait))
            {
                return true;
            }

            return false;
        }

        private static bool ResolveUIInteractions()
        {
            if (DetectAction(EnumAction.PauseMenu))
            {
                XnaUI.CurrentUIMenu = new PauseMenu();
                return false;
            }

            if (DetectAction(EnumAction.CommandInputOn))
            {
                XnaUI.CurrentUIMenu = new ConsoleInputMenu();
                return false;
            }

            if (DetectAction(EnumAction.Interact))
            {
                XnaUI.CurrentUIMenu = new InteractMenu();
                return false;
            }

            return true;
        }

        private static bool DetectAction(EnumAction action)
        {
            var newlyPressed = KeyboardInputHelper.CurrentState.GetPressedKeys().Where(key => KeyboardInputHelper.PreviousState.IsKeyUp(key)).ToList();
            if (!KeyMappings.ContainsKey(action)) return false;

            var actionMapping = KeyMappings[action];
            return actionMapping.All(newlyPressed.Contains);
        }

        private static bool DetectActionWithoutBuffering(EnumAction action)
        {
            if (!KeyMappings.ContainsKey(action)) return false;

            var actionMapping = KeyMappings[action];
            return actionMapping.All(KeyboardInputHelper.CurrentState.GetPressedKeys().Contains);
        }

        private static EnumDirection DetectDirectionKey()
        {
            return DetectActionWithoutBuffering(EnumAction.GoUp)
                ? EnumDirection.Up
                : (DetectActionWithoutBuffering(EnumAction.GoLeft))
                    ? EnumDirection.Left
                    : (DetectActionWithoutBuffering(EnumAction.GoDown)
                        ? EnumDirection.Down
                        : (DetectActionWithoutBuffering(EnumAction.GoRight) ? EnumDirection.Right : EnumDirection.Nowhere));
        }
    }
}
