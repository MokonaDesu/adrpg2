﻿using Microsoft.Xna.Framework.Graphics;

namespace AdRPG.XnaClient.Renderers
{
    class GameRenderer
    {
     
        public static void Render(SpriteBatch spriteBatch)
        {
            LocationRenderer.Render(spriteBatch);
            UIRenderer.Render(spriteBatch);
        }
    }

}
