﻿using System;
using System.Globalization;
using AdRPG.Logic.World.Loc;
using AdRPG.Logic.World.Tiles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game = AdRPG.Logic.Game.Game;

namespace AdRPG.XnaClient.Renderers
{
    class LocationRenderer
    {
        private Game gameInstance = Game.Instance;
        static Random random = new Random();

        public static void Render(SpriteBatch spriteBatch)
        {
            var loc = Game.Instance.CurrentLoc;
            var player = Game.Instance.CurrentPlayer;

            var cameraX = player.Hero.X;
            var cameraY = player.Hero.Y;

            for (int y = -player.Hero.VisionRadius;
                y < player.Hero.VisionRadius; y++)
            {
                for (int x = -player.Hero.VisionRadius;
                    x < player.Hero.VisionRadius; x++)
                {
                    if (Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2)) <= player.Hero.VisionRadius)
                    {
                        var absX = cameraX + x;
                        var absY = cameraY + y;

                        var bufferX = x + player.Hero.VisionRadius;
                        var bufferY = y + player.Hero.VisionRadius;

                        if (ValidPoint(loc, absX, absY))
                        {
                            Tile tile = TileAtlas.Instance.GetById(loc.TileId(absX, absY));
                            byte variant = loc.GetTileVariant(absX, absY);

                            spriteBatch.Draw(TextureAtlas.GetTexture(tile.GetTextureId(variant)),
                                new Rectangle(Client.Width / 2 + 16 * (bufferX - player.Hero.VisionRadius),
                                    Client.Height / 2 + 16 * (bufferY - player.Hero.VisionRadius), 16, 16),
                                    tile.GetColor(variant));
                        }
                    }
                }
            }

            foreach (var entity in loc.Entities)
            {
                EntityRenderer.Render(spriteBatch, entity);
            }
        }

        private static bool ValidPoint(Location loc, int x, int y)
        {
            return !(x < 0 || y < 0 || x >= loc.SizeX || y >= loc.SizeY);
        }

    }
}
