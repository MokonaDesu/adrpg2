﻿using AdRPG.Logic.World.Tiles;
using AdRPG.XnaClient.UI;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace AdRPG.XnaClient.Renderers
{
    class TextureAtlas
    {
        private static readonly Texture2D[] Textures = new Texture2D[256];

        public static void LoadTextures(ContentManager manager)
        {
            Textures[TileVoid.TextureId] = manager.Load<Texture2D>("textures/tileVoid");

            #region [ TileFloor textures ]
            Textures[TileFloor.TextureId] = manager.Load<Texture2D>("textures/tileFloor_0");
            Textures[TileFloor.Texture2Id] = manager.Load<Texture2D>("textures/tileFloor_1");
            Textures[TileFloor.Texture3Id] = manager.Load<Texture2D>("textures/tileFloor_2");
            Textures[TileFloor.Texture4Id] = manager.Load<Texture2D>("textures/tileFloor_3");
            #endregion

            #region [ TileWall textures ]
            Textures[TileWall.TopLeftCornerTextureId] = manager.Load<Texture2D>("textures/tileWallTopLeft");
            Textures[TileWall.TopRightCornerTextureId] = manager.Load<Texture2D>("textures/tileWallTopRight");
            Textures[TileWall.BottomLeftCornerTextureId] = manager.Load<Texture2D>("textures/tileWallBottomLeft");
            Textures[TileWall.BottomRightCornerTextureId] = manager.Load<Texture2D>("textures/tileWallBottomRight");
            Textures[TileWall.HorizontalTextureId] = manager.Load<Texture2D>("textures/tileWallHorizontal");
            Textures[TileWall.VerticalTextureId] = manager.Load<Texture2D>("textures/tileWallVertical");
            Textures[TileWall.HorizontalDownTextureId] = manager.Load<Texture2D>("textures/tileWallHorizontalDown");
            Textures[TileWall.HorizontalUpTextureId] = manager.Load<Texture2D>("textures/tileWallHorizontalUp");
            Textures[TileWall.VerticalLeftTextureId] = manager.Load<Texture2D>("textures/tileWallVerticalLeft");
            Textures[TileWall.VerticalRightTextureId] = manager.Load<Texture2D>("textures/tileWallVerticalRight");
            Textures[TileWall.CrossTextureId] = manager.Load<Texture2D>("textures/tileWallCross");
            #endregion

            #region [ TileLava textures ]
            Textures[TileLava.LavaTextureBase] = manager.Load<Texture2D>("textures/tileLava_0");
            Textures[TileLava.LavaTextureBase + 1] = manager.Load<Texture2D>("textures/tileLava_1");
            Textures[TileLava.LavaTextureBase + 2] = manager.Load<Texture2D>("textures/tileLava_2");
            Textures[TileLava.LavaTextureBase + 3] = manager.Load<Texture2D>("textures/tileLava_3");
            Textures[TileLava.LavaTextureBase + 4] = manager.Load<Texture2D>("textures/tileLava_4");
            Textures[TileLava.LavaTextureBase + 5] = manager.Load<Texture2D>("textures/tileLava_5");
            Textures[TileLava.LavaTextureBase + 6] = manager.Load<Texture2D>("textures/tileLava_6");
            Textures[TileLava.LavaTextureBase + 7] = manager.Load<Texture2D>("textures/tileLava_7");
            #endregion

            #region [ FX textures ]

            Textures[InteractMenu.InteractableFxTextureIndex] = manager.Load<Texture2D>("textures/fx/interactionTarget");

            #endregion
        }

        public static Texture2D GetTexture(int id)
        {
            if (id >= 0 && id < 256)
                return Textures[id];
            return null;
        }
    }
}
