﻿using AdRPG.XnaClient.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AdRPG.XnaClient.Renderers
{
    class UIRenderer
    {
        public static void Render(SpriteBatch spriteBatch)
        {
            if (XnaUI.CurrentUIMenu != null)
            {
                XnaUI.CurrentUIMenu.Render(spriteBatch);    
            }

            var line = 0;

            foreach (string message in XnaUI.Console.Messages)
            {
                if (message != null)
                {
                    spriteBatch.DrawString(Client.Consolas, message,
                        new Vector2(XnaConsoleConstants.PositionX,
                            XnaConsoleConstants.PositionY + XnaConsoleConstants.LineHeight*line), Color.White);
                    line++;
                }
            }
        }
    }
}
