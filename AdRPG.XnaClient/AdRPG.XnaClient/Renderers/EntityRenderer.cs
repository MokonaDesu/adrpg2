﻿using System;
using System.Globalization;
using AdRPG.Logic.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game = AdRPG.Logic.Game.Game;

namespace AdRPG.XnaClient.Renderers
{
    class EntityRenderer
    {
        public static void Render(SpriteBatch spriteBatch, Entity entity)
        {
            var hero = Game.Instance.CurrentPlayer.Hero;

            var entityRelativeX = entity.X - hero.X;
            var entityRelativeY = entity.Y - hero.Y;

            if (Math.Sqrt(Math.Pow(entityRelativeX, 2) + Math.Pow(entityRelativeY, 2)) <= hero.VisionRadius)
            {
                spriteBatch.DrawString(Client.Consolas, entity.GetDrawing().ToString(CultureInfo.InvariantCulture),
                                new Vector2(Client.Width / 2 + 16 * (entityRelativeX + hero.VisionRadius - hero.VisionRadius),
                                    Client.Height / 2 + 16 * (entityRelativeY + hero.VisionRadius - hero.VisionRadius)), Color.Beige);
            }
        }
    }
}
