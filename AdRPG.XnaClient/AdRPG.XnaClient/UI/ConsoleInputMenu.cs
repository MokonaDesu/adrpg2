﻿using AdRPG.Logic.Game.ConsoleCommands;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AdRPG.XnaClient.UI
{
    class ConsoleInputMenu : UIMenu
    {
        public string CurrentText { get; private set; }

        public override UIMenuResult ResolveInput()
        {
            if (KeyboardInputHelper.DetectNewKeypress(Keys.Enter))
            {
                if (!string.IsNullOrWhiteSpace(CurrentText))
                    XnaUI.Console.AddMessage(ConsoleCommand.ExecuteCommandByName(CurrentText));
                return new UIMenuResult(true, false);
            }

            if (KeyboardInputHelper.DetectNewKeypress(Keys.Escape))
            {
                return new UIMenuResult(true, false);
            }

            AppendKeyboardInput();
            ResolveBackspace();
            return new UIMenuResult(false, false);
        }

        private void ResolveBackspace()
        {
            if (KeyboardInputHelper.DetectNewKeypress(Keys.Back))
            {
                if (!string.IsNullOrEmpty(CurrentText))
                {
                    CurrentText = CurrentText.Substring(0, CurrentText.Length - 1);
                }
            }
        }

        private void AppendKeyboardInput()
        {
            var pressed = KeyboardInputHelper.ConvertKeyboardInput();
            if (pressed != 0)
            {
                CurrentText = CurrentText + pressed;
            }
        }

        public override void Render(SpriteBatch sb)
        {
            sb.DrawString(Client.Consolas, ">" + CurrentText, new Vector2(10, 10), Color.White);
        }
    }
}
