﻿using System;
using AdRPG.Logic.Game.ConsoleCommands;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AdRPG.XnaClient.UI
{
    class PauseMenu : UIMenu
    {
        private readonly Tuple<string, string>[] actions =
        {
            new Tuple<string, string>("Resume", string.Empty),
            new Tuple<string, string>("Save", "save"),
            new Tuple<string, string>("Load", "load"),
            new Tuple<string, string>("Quit", "quit")
        };

        private int currentAction = 0;

        public override UIMenuResult ResolveInput()
        {
            if (KeyboardInputHelper.DetectNewKeypress(Keys.Down))
            {
                currentAction++;
                if (currentAction == actions.Length) currentAction = 0;
            }

            if (KeyboardInputHelper.DetectNewKeypress(Keys.Up))
            {
                currentAction--;
                if (currentAction == -1) currentAction = actions.Length - 1;
            }

            if (KeyboardInputHelper.DetectNewKeypress(Keys.Escape))
            {
                return new UIMenuResult(true, false);
            }

            if (KeyboardInputHelper.DetectNewKeypress(Keys.Enter))
            {
                if (actions[currentAction].Item2 == string.Empty)
                {
                    return new UIMenuResult(true, false);
                }

                XnaUI.Console.AddMessage(ConsoleCommand.ExecuteCommandByName(actions[currentAction].Item2));
                return new UIMenuResult(true, true);
            }

            return new UIMenuResult(false, false);
        }

        public override void Render(SpriteBatch sb)
        {
            const int baseX = 10;
            const int baseY = 10;
            const int lineHeight = 15;

            for (var i = 0; i < actions.Length; i++)
            {
                sb.DrawString(Client.Consolas, actions[i].Item1, new Vector2(baseX, baseY + i * lineHeight), i == currentAction ? Color.Red : Color.White);
            }
        }
    }
}
