﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AdRPG.XnaClient.UI
{
    public class UIMenuResult
    {
        public bool ExitMenu { get; set; }

        public bool NeedUpdate { get; set; }

        public UIMenuResult(bool exitMenu, bool needUpdate)
        {
            ExitMenu = exitMenu;
            NeedUpdate = needUpdate;
        }
    }

    class UIMenu
    {
        public virtual UIMenuResult ResolveInput()
        {
            return KeyboardInputHelper.CurrentState.IsKeyDown(Keys.Escape) ? new UIMenuResult(true, false) : new UIMenuResult(false, false);
        }

        public virtual void Render(SpriteBatch sb)
        {
            sb.DrawString(Client.Consolas, "In the menu...", new Vector2(10, 10), Color.RoyalBlue);
        }
    }
}
