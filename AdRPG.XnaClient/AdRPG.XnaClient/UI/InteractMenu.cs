﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdRPG.Logic;
using AdRPG.Logic.Entities;
using AdRPG.Logic.Game;
using AdRPG.Logic.World.Tiles;
using AdRPG.XnaClient.Renderers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Game = AdRPG.Logic.Game.Game;

namespace AdRPG.XnaClient.UI
{
    class InteractableContainer
    {
        
    }

    class InteractMenu : UIMenu
    {
        private readonly IInteractable[] interactables;

        private EnumInteraction[] CurrentAvailableActions
        {
            get
            {
                return interactables[currentInteractableIndex]
                    .GetPossibleInteractions(Game.Instance.CurrentPlayer.Hero).ToArray();
            }
        }

        private EnumInteraction CurrentSelectedInteraction
        {
            get { return CurrentAvailableActions[currentActionIndex]; }
        }

        private IInteractable CurrentInteractionTarget
        {
            get { return interactables[currentInteractableIndex]; }
        }

        private int currentInteractableIndex;
        private int currentActionIndex;

        public const int InteractableFxTextureIndex = 27;

        public bool InteractableEntity(Entity e)
        {
            EntityPlayer entityPlayer = Game.Instance.CurrentPlayer.Hero;

            return e.GetPossibleInteractions(entityPlayer).Any() && !(e is EntityPlayer);
        }

        public static double DistanceBetweenEntities(Entity e1, Entity e2)
        {
            return Math.Sqrt(Math.Pow(e1.X - e2.X, 2) + Math.Pow(e1.Y - e2.Y, 2));
        }

        private IEnumerable<IInteractable> GetInteractableItems()
        {
            var result = new List<IInteractable>();

            result.AddRange(GetInteractableEntities());
            result.AddRange(GetInteractableTiles());

            return result;
        }

        private IEnumerable<Entity> GetInteractableEntities()
        {
            var playerEntity = Game.Instance.CurrentPlayer.Hero;
            return Game.Instance.CurrentLoc.Entities.Where(InteractableEntity).OrderBy(e => DistanceBetweenEntities(e, playerEntity));
        }

        private IEnumerable<IInteractable> GetInteractableTiles()
        {
            var tiles = new List<IInteractable>();
            var playerEntity = Game.Instance.CurrentPlayer.Hero;

            for (var i = -(int)GameRules.MaxInterationDistance; i < GameRules.MaxInterationDistance; i++)
            {
                for (var j = -(int) GameRules.MaxInterationDistance; j < GameRules.MaxInterationDistance; j++)
                {
                    var tile = TileAtlas.Instance.GetById(Game.Instance.CurrentLoc.TileId(playerEntity.X + i, playerEntity.Y + j));
                    var item = tile as IInteractableTile;
                    var tileVariant = Game.Instance.CurrentLoc.GetTileVariant(playerEntity.X + i, playerEntity.Y + j);
                    if (item != null && item.GetPossibleInteractions(playerEntity.X + i, playerEntity.Y + j, playerEntity, tileVariant).Any())
                    {
                        tiles.Add(new InteractableTileAdapter(item, playerEntity.X + i, playerEntity.Y + j));
                    }
                }
            }

            return tiles;
        }

        public InteractMenu()
        {
            interactables = GetInteractableItems().ToArray();
        }

        public override void Render(SpriteBatch sb)
        {
            if (!interactables.Any())
                return;

            var entityRelativeX = interactables[currentInteractableIndex].X - Game.Instance.CurrentPlayer.Hero.X;
            var entityRelativeY = interactables[currentInteractableIndex].Y - Game.Instance.CurrentPlayer.Hero.Y;

            var currentItemDrawPosision =
                new Vector2(
                    Client.Width/2 +
                    16*
                    (entityRelativeX + Game.Instance.CurrentPlayer.Hero.VisionRadius -
                     Game.Instance.CurrentPlayer.Hero.VisionRadius),
                    Client.Height/2 +
                    16*
                    (entityRelativeY + Game.Instance.CurrentPlayer.Hero.VisionRadius -
                     Game.Instance.CurrentPlayer.Hero.VisionRadius));

            sb.Draw(TextureAtlas.GetTexture(InteractableFxTextureIndex), currentItemDrawPosision, Color.White);
            sb.DrawString(Client.Consolas, CurrentSelectedInteraction.ToString(), new Vector2(10, 10), Color.White);
        }

        public override UIMenuResult ResolveInput()
        {
            if (!interactables.Any())
            {
                XnaUI.Console.AddMessage("Nothing to interact with...");
                return new UIMenuResult(true, false);
            }

            if (KeyboardInputHelper.DetectNewKeypress(Keys.Right))
            {
                currentInteractableIndex++;
                currentActionIndex = 0;
                if (currentInteractableIndex == interactables.Length) currentInteractableIndex = 0;
            }

            if (KeyboardInputHelper.DetectNewKeypress(Keys.Left))
            {
                currentInteractableIndex--;
                currentActionIndex = 0;
                if (currentInteractableIndex == -1) currentInteractableIndex = interactables.Length - 1;
            }

            if (KeyboardInputHelper.DetectNewKeypress(Keys.Down))
            {
                currentActionIndex++;
                if (currentActionIndex == CurrentAvailableActions.Length) currentActionIndex = 0;
            }

            if (KeyboardInputHelper.DetectNewKeypress(Keys.Up))
            {
                currentActionIndex--;
                if (currentActionIndex == -1) currentActionIndex = CurrentAvailableActions.Length - 1;
            }

            if (KeyboardInputHelper.DetectNewKeypress(Keys.Escape))
            {
                return new UIMenuResult(true, false);
            }

            if (KeyboardInputHelper.DetectNewKeypress(Keys.Enter))
            {
                var executionResult = CurrentInteractionTarget.OnInteract(Game.Instance.CurrentPlayer.Hero, CurrentSelectedInteraction);

                executionResult.Messages.ForEach(m => XnaUI.Console.AddMessage(m));

                return new UIMenuResult(true, executionResult.Success);
            }

            return new UIMenuResult(false, false);
        }
    }
}
