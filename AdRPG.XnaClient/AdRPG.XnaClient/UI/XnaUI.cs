﻿namespace AdRPG.XnaClient.UI
{
    class XnaUI
    {
        public static XnaGameConsole Console { get; set; }
        public static string CommandInputString { get; private set; }
        public static UIMenu CurrentUIMenu { get; set; }

        static XnaUI()
        {
            Console = new XnaGameConsole();
        }
    }
}
