﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Xml.Linq;

namespace AdRPG.XnaClient
{

    public static class XnaConsoleConstants
    {
        public const int PositionX = 50;
        public const int PositionY = 565;
        public const int LineHeight = 18;
        public const int ConsoleBufferSize = 10;
        public const int BufferLength = 80;
    }

    public class XnaGameConsole
    {
        private readonly List<string> prevMessages;
        private readonly List<string> nextMessages; 
        private readonly string[] messages;
        public IEnumerable Messages
        {
            get { return messages; }
        }

        public XnaGameConsole()
        {
            messages = new string[XnaConsoleConstants.ConsoleBufferSize];
        }

        public void AddMessage(string message)
        {
            if (message.Length > XnaConsoleConstants.BufferLength)
            {
                string first, second;
                SplitFirstLine(message, out first, out second);
                AddMessage(first);
                AddMessage(second);
            }
            else
            {
                for (int i = 0; i < XnaConsoleConstants.ConsoleBufferSize - 1; i++)
                {
                    messages[i] = messages[i + 1];
                }

                messages[XnaConsoleConstants.ConsoleBufferSize - 1] = message +
                                                              new string(' ',
                                                                  XnaConsoleConstants.BufferLength -
                                                                  message.Length);
            }

        }

        private void SplitFirstLine(string str, out string str1, out string str2)
        {
            var words = str.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            str1 = str2 = string.Empty;
            
            if (words.Count() == 1)
            {
                str1 = str.Substring(0, XnaConsoleConstants.BufferLength);
                str2 = str.Substring(XnaConsoleConstants.BufferLength);
            }

            var builder = new StringBuilder();
            var length = 0;
            for (int i = 0; i < words.Count(); i++)
            {
                if (length + words[i].Length > XnaConsoleConstants.BufferLength - 1)
                {
                    str1 = builder.ToString();
                    str2 = String.Join(" ", words.Skip(i));

                    return;
                }

                builder.AppendFormat("{0} ", words[i]);
                length += words[i].Length + 1;
            }
        }
    }
}
