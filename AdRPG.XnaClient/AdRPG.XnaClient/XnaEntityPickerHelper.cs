﻿using System.Collections.Generic;
using System.Linq;
using AdRPG.Logic.Game;
using AdRPG.Logic.World;
using AdRPG.XnaClient.UI;
using Microsoft.Xna.Framework.Input;

namespace AdRPG.XnaClient
{
    class XnaEntityPickerHelper
    {
        public static T PickEntityFrom<T>(IEnumerable<T> itemsToInteract, Keys keyPressed)
        {
            int currentPage = 0;
            var interactableGroups = itemsToInteract.GroupBy(XnaConsoleConstants.ConsoleBufferSize - 1);

            while (true)
            {
                var currentItemGroup = interactableGroups.Skip(currentPage).First().ToList();

                RenderPicker(currentItemGroup, currentPage);

                return default(T);

                /*
                var pressedChar = char.ToLowerInvariant(System.Console.ReadKey(true).KeyChar);

                if (pressedChar > '0' && pressedChar <= '9')
                {
                    if (currentItemGroup.Count >= pressedChar - '1')
                    {
                        ClearConsole();
                        return currentItemGroup[pressedChar - '1'];
                    }
                }
                else
                {
                    if (pressedChar == '+')
                    {
                        currentPage = interactableGroups.Count() > currentPage + 1 ? currentPage + 1 : 0;
                    }
                    else if (pressedChar == '-')
                    {
                        currentPage = currentPage == 0 ? interactableGroups.Count() - 1 : currentPage - 1;
                    }
                    else if (pressedChar == 'q')
                    {
                        ClearConsole();
                        return default(T);
                    }
                }
            */
            }
        }

        private static void RenderPicker<T>(List<T> currentItemGroup, int currentPage)
        {
            XnaUI.Console.AddMessage(string.Format("[ Page {0} ]", currentPage + 1));
            AddPickerMessages(currentItemGroup);
            FillBlankSpots(currentItemGroup);
        }

        private static void FillBlankSpots<T>(List<T> currentItemGroup)
        {
            for (int i = 0; i < XnaConsoleConstants.ConsoleBufferSize - currentItemGroup.Count - 1; i++)
            {
                XnaUI.Console.AddMessage(" ");
            }
        }

        private static void AddPickerMessages<T>(List<T> currentItemGroup)
        {
            for (int i = 0; i < currentItemGroup.Count; i++)
            {
                if (currentItemGroup.Count > i)
                {
                    XnaUI.Console.AddMessage((i + 1) + ") " +
                                                                   ObjectAtlas.NameFor(currentItemGroup[i]));
                }
            }
        }

        private static void ClearConsole()
        {
            for (int i = 0; i < XnaConsoleConstants.ConsoleBufferSize; i++)
                XnaUI.Console.AddMessage("");
        }
    }
}
