using System.Collections.Generic;
using System.Runtime.InteropServices;
using AdRPG.Logic.World.Tiles;
using AdRPG.XnaClient.Renderers;
using AdRPG.XnaClient.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AdRPG.XnaClient
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Client : Game
    {
        public static SpriteFont Consolas;

        public static int UpdateCooldown = 100;

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private readonly Logic.Game.Game gameInstance;
        private bool gameNeedsUpdate = true;

        public static int Width = 1024;
        public static int Height = 768;

        public Client()
        {
            graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferHeight = Height,
                PreferredBackBufferWidth = Width
            };

            gameInstance = Logic.Game.Game.Instance;

            Content.RootDirectory = "assets";
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            spriteBatch = new SpriteBatch(GraphicsDevice);

            TextureAtlas.LoadTextures(Content);

            Consolas = Content.Load<SpriteFont>("fonts/Consolas");
        }

        private int sinceActualUpdate = 0;
        private bool ReadyToUpdate(GameTime gameTime)
        {
            sinceActualUpdate += gameTime.ElapsedGameTime.Milliseconds;
            return sinceActualUpdate > UpdateCooldown;
        }

        protected override void Update(GameTime gameTime)
        {
            if (gameInstance.Exiting)
            {
                Exit();    
            }

            gameNeedsUpdate = ReadyToUpdate(gameTime) && XnaInputResolver.ResolvePlayerInput(gameTime);
            KeyboardInputHelper.UpdateState();

            if (gameNeedsUpdate)
            {
                sinceActualUpdate = 0;
                var updateMessages = new List<string>();
                gameInstance.Update(updateMessages);
                updateMessages.ForEach(m => XnaUI.Console.AddMessage(m));
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            TileLava.AnimationStep();

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            GameRenderer.Render(spriteBatch);
            spriteBatch.End();
        }
    }
}