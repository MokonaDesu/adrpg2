﻿using System;
using AdRPG.Logic.Entities;
using Microsoft.Xna.Framework;

namespace AdRPG.Logic.World.Tiles
{
    public abstract class Tile 
    {
        public int Id { get; protected set; }

        public static Random TileRandomizer = new Random();

        protected Tile(int id)
        {
            Id = id;
        }

        public virtual int GetTextureId(byte variant)
        {
            return 0;
        }

        public virtual Color GetColor(byte variant)
        {
            return Color.White;
        }

        public virtual bool CanPass(Entity e, byte variant)
        {
            return false;
        }
    }
}
