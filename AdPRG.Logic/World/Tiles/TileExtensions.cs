﻿namespace AdRPG.Logic.World.Tiles
{
    static class TileExtensions
    {
        public static Tile Register(this Tile tile) {
            TileAtlas.Instance.RegisterTile(tile);
            return tile;
        }

    }
}
