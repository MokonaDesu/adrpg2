﻿using AdRPG.Logic.Entities;

namespace AdRPG.Logic.World.Tiles
{
    public class TileVoid : Tile
    {
        public static int TextureId = 0;

        public TileVoid(int id) : base(id) { }

        public override bool CanPass(Entity entity, byte variant)
        {
            return false;
        }

        public override int GetTextureId(byte variant)
        {
            return TextureId;
        }
    }
}
