﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdRPG.Logic.Entities;
using AdRPG.Logic.Game;
using AdRPG.Logic.Items;

namespace AdRPG.Logic.World.Tiles
{
    public class TileDoor : Tile, IInteractableTile
    {
        public const int VariantDoorClosed = 2;
        public const int VariantDoorBroken = 1;
        public const int VariantDoorOpen = 0;

        public TileDoor(int id) : base(id)
        {
        }

        public override bool CanPass(Entity entity, byte variant)
        {
            return variant != VariantDoorClosed;
        }

        public IList<EnumInteraction> GetPossibleInteractions(int coordX, int coordY, EntityLiving e, byte variant)
        {
            var distance = Math.Sqrt(Math.Pow(e.X - coordX, 2) + Math.Pow(e.Y - coordY, 2));

            return distance <= GameRules.MaxInterationDistance ? new List<EnumInteraction> {EnumInteraction.Open, EnumInteraction.Close, EnumInteraction.Break} : new List<EnumInteraction>();
        }

        public EntityActionResult OnInteract(int coordX, int coordY, EntityLiving e, EnumInteraction interactionCode, byte variant)
        {
            switch (interactionCode)
            {
                case EnumInteraction.Open:
                {
                    return AttemptOpenDoor(coordX, coordY, e, variant);
                }
                case EnumInteraction.Close:
                {
                    return AttemptCloseDoor(coordX, coordY, variant);
                }
            }

            return new EntityActionResult {Success = true}.AddMessage(string.Format("Attempted to {0} the door!", interactionCode));
        }

        private EntityActionResult AttemptCloseDoor(int coordX, int coordY, byte variant)
        {
            var actionResult = new EntityActionResult();

            if (variant == VariantDoorClosed)
            {
                actionResult.Success = false;
                actionResult.Messages.Add("Door is already closed!");
            }
            else
            {
                Game.Game.Instance.CurrentLoc.SetTileVariant(coordX, coordY, VariantDoorClosed);
                actionResult.Success = true;
                actionResult.Messages.Add("Closed the door!");
            }

            return actionResult;
        }

        private EntityActionResult AttemptOpenDoor(int coordX, int coordY, EntityLiving e, byte variant)
        {
            var actionResult = new EntityActionResult();

            if (variant == VariantDoorClosed)
            {
                var player = e as EntityPlayer;
                if (player != null)
                {
                    var item = player.Items.FirstOrDefault(i => i is ItemKey);
                    if (item != null)
                    {
                        player.Items.Remove(item);
                        Game.Game.Instance.CurrentLoc.SetTileVariant(coordX, coordY, VariantDoorOpen);
                        actionResult.Success = true;
                        actionResult.Messages.Add("You opened a door!");
                    }
                    else
                    {
                        actionResult.Success = false;
                        actionResult.Messages.Add("You need a key to open the door!");
                    }
                }
            }
            else
            {
                actionResult.Success = false;
                actionResult.Messages.Add("Door is already open!");
            }


            return actionResult;
        }
    }
}
