﻿using AdRPG.Logic.Entities;
using Microsoft.Xna.Framework;

namespace AdRPG.Logic.World.Tiles
{
    public class TileLava : Tile
    {
        public const int LavaTextureBase = 19;
        public int[] LavaTextureIds = { 19, 20, 21, 22, 23, 24, 25, 26 };

        private static int animationPhase;
        private static int updates;

        private const int UpdatesPerFrame = 30;
        private const int AnimationFrames = 8;

        public TileLava(int id) : base(id)
        {

        }

        public override int GetTextureId(byte variant)
        {
            return LavaTextureIds[animationPhase];
        }

        public static void AnimationStep()
        {
            updates++;

            if (updates >= UpdatesPerFrame)
            {
                updates = 0;
                animationPhase++;
            }

            if (animationPhase >= AnimationFrames)
            {
                animationPhase = 0;
            }
        }

        public override Color GetColor(byte variant)
        {
            return Color.White;
        }

        public override bool CanPass(Entity entity, byte variant)
        {
            return false;
        }
    }
}
