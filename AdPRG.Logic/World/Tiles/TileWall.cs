﻿using System;

namespace AdRPG.Logic.World.Tiles
{
    public class TileWall : Tile
    {
        public static int VerticalTextureId = 2;
        public static int HorizontalTextureId = 3;
        public static int TopLeftCornerTextureId = 4;
        public static int TopRightCornerTextureId = 5;
        public static int BottomRightCornerTextureId = 6;
        public static int BottomLeftCornerTextureId = 7;
        public static int VerticalRightTextureId = 8;
        public static int VerticalLeftTextureId = 9;
        public static int HorizontalDownTextureId = 10;
        public static int HorizontalUpTextureId = 11;
        public static int CrossTextureId = 12;

        public TileWall(int id) : base(id) { }
        public override int GetTextureId(byte variant)
        {
            return variant%16 + 2;
        }
    }
}
