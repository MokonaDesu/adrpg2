﻿using System;
using AdRPG.Logic.Entities;

namespace AdRPG.Logic.World.Tiles
{
    public class TileFloor : Tile
    {
        public TileFloor(int id) : base(id) { }

        public static int TextureId = 1;
        public static int Texture2Id = 13;
        public static int Texture3Id = 14;
        public static int Texture4Id = 15;

        public override bool CanPass(Entity entity, byte variant)
        {
            return true;
        }

        public override int GetTextureId(byte variant)
        {
            return variant % 4 == 0 ? TextureId : variant % 4 == 1 ? Texture2Id : variant % 4 == 2 ? Texture3Id : Texture4Id;
        }
    }
}
