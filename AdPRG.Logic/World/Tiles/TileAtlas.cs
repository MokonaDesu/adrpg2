﻿using System;
using AdRPG.Logic.Game;

namespace AdRPG.Logic.World.Tiles
{
    public class TileAtlas
    {

        #region [ Singleton ]
        private static TileAtlas instance;
        public static TileAtlas Instance
        {
            get
            {
                return instance ?? (instance = new TileAtlas());
            }
        }
        #endregion

        #region [ Tiles ]
        public TileVoid TileVoid = new TileVoid(0);
        public TileFloor TileFloor = new TileFloor(1);
        public TileWall TileWall = new TileWall(2);
        public TileDoor TileDoor = new TileDoor(3);
        public TileLava TileLava = new TileLava(5);
        #endregion

        #region [ Registering Tiles ]
        public void RegisterTiles()
        {
            ObjectAtlas.RegisterName(TileVoid, "???");
            ObjectAtlas.RegisterName(TileFloor, "Floor");
            ObjectAtlas.RegisterName(TileWall, "Wall");
            ObjectAtlas.RegisterName(TileDoor, "Door");
            ObjectAtlas.RegisterName(TileLava, "Lava");

            RegisterTile(TileVoid, TileFloor, TileWall, TileDoor, TileLava);
        }
        #endregion

        private TileAtlas()
        {
            RegisterTiles();
        }

        private readonly Tile[] tiles = new Tile[128];

        public void RegisterTile(params Tile[] t)
        {
            foreach (var tile in t)
            {
                if (tiles[tile.Id] == null)
                {
                    tiles[tile.Id] = tile;
                }
                else
                {
                    throw new ArgumentException("The Atlas already contains a tile with such ID!");
                }
            }
        }

        public Tile GetById(int id)
        {
            return tiles[id];
        }

    }
}
