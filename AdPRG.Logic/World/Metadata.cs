﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdRPG.Logic.World
{
    [Serializable]
    public class Metadata
    {
        private readonly IDictionary<string, object> data = new Dictionary<string, object>();

        public IEnumerable<object> Items { get { return data.Values.AsEnumerable(); } } 

        public T Read<T>(string propertyName)
        {
            try
            {
                return (T)data[propertyName];
            }
            catch
            {
                return default(T);
            }
        }

        public bool Write<T>(string propertyName, T value)
        {
            try
            {
                data[propertyName] = value;
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
