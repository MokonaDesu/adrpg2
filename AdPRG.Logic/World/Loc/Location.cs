﻿using System.Runtime.Serialization;
using AdRPG.Logic.Entities;
using System.Collections.Generic;

namespace AdRPG.Logic.World.Loc
{
    [DataContract]
    public class Location
    {

        #region [ Properties ]

        [DataMember]
        public string Name { get; private set; }

        [DataMember(Name = "Tiles")]
        private readonly int[][] tiles;
        [DataMember(Name = "Variants")]
        private readonly byte[][] variants;

        private Metadata metadata;
        private IList<Entity> entities;

        [DataMember(Name = "SpawnX")]
        public int SpawnPositionX { get; set; }
        [DataMember(Name = "SpawnY")]
        public int SpawnPositionY { get; set; }
        public IEnumerable<Entity> Entities { get { return entities; } }

        [DataMember(Name = "SizeX")]
        public int SizeX { get; private set; }
        [DataMember(Name = "SizeY")]
        public int SizeY { get; private set; }

        #endregion

        public Location(string name, int xSize, int ySize, int[][] tileMap = null, byte[][] variantMap = null)
        {
            Name = name;
            if (tileMap == null)
            {
                tiles = new int[xSize][];
                for (int i = 0; i < xSize; i++)
                {
                    tiles[i] = new int[ySize];
                }
            }
            else
            {
                tiles = tileMap;
            }

            if (variantMap == null)
            {
                variants = new byte[xSize][];
                for (int i = 0; i < xSize; i++)
                {
                    variants[i] = new byte[ySize];
                }
            }
            else
            {
                variants = variantMap;
            }

            PreInit();
            SizeX = xSize;
            SizeY = ySize;
            SpawnPositionX = SizeX / 2;
            SpawnPositionY = SizeY / 2;
        }

        public Location PreInit()
        {
            metadata = metadata ?? new Metadata();
            entities = entities ?? new List<Entity>();
            return this;
        }

        public int TileId(int x, int y)
        {
            return tiles[x][y];
        }

        public Metadata GetTileMetadata(int x, int y)
        {
            return metadata.Read<Metadata>("tile_" + x + "_" + y);
        }

        public void SetTileId(int x, int y, int id)
        {
            tiles[x][y] = id;
        }

        public void SetTileMetadata(int x, int y, Metadata data)
        {
            metadata.Write<Metadata>("tile_" + x + "_" + y, data);
        }

        public byte GetTileVariant(int x, int y)
        {
            return variants[x][y];
        }

        public void SetTileVariant(int x, int y, byte variant)
        {
            variants[x][y] = variant;
        }

        public void SpawnEntity(Entity entity, int xCoord = 1, int yCoord = 1)
        {
            entity.X = xCoord;
            entity.Y = yCoord;
            entity.Location = this;

            entities.Add(entity);
        }

        public void UpdateEntityList()
        {
            for (int i = 0; i < entities.Count; i++)
            {
                if (entities[i] is EntityLiving && ((EntityLiving)entities[i]).IsDead)
                {
                    entities.RemoveAt(i);
                    i--;
                }
            }
        }
    }
}
