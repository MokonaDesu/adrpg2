﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AdRPG.Logic.Entities;
using AdRPG.Logic.World.Loc;
using AdRPG.Logic.World.Tiles;

namespace AdRPG.Logic.World.Generation
{
    class DungeonGenerator : ILocationGenerator
    {
        #region [ Private fields ]
        private Random random;
        private EnumRooms[,] generatedRooms;
        private int[,] generatedLocation;
        private byte[,] generatedLocationVariants;
        private const int MaxRoomsX = 15;
        private const int MaxRoomsY = 15;
        private const int RoomSizeX = 15;
        private const int RoomSizeY = 15;
        private Tuple<int, int> spawnRoom;

        private const float MobChance = 0.8f;

        private readonly List<Tuple<Tuple<int, int>, Entity>> spawningQueue =
            new List<Tuple<Tuple<int, int>, Entity>>();
        #endregion

        #region [ Debug methods ]
        public static void WriteMazeToFile(EnumRooms[,] maze, string filename)
        {
            using (TextWriter fs = File.CreateText(filename))
            {
                for (int y = 0; y < MaxRoomsY; y++)
                {
                    for (int x = 0; x < MaxRoomsX; x++)
                    {
                        fs.Write((maze[x, y] == EnumRooms.NoRoom) ? "█" : " ");
                    }
                    fs.Write("\n");
                }
            }
        }
        #endregion

        public Location Generate(int? seed = null)
        {
            try
            {
                return GenerateLocation(seed);
            }
            catch (Exception e)
            {
                Console.Write(e.StackTrace);
                return null;
            }
        }

        private Location GenerateLocation(int? seed)
        {
            SetupRandomizer(seed);
            GenerateRoomMaze();
            DetailLocation();

            var loc = new Location("Dungeon", MaxRoomsX * (RoomSizeX - 1), MaxRoomsY * (RoomSizeY - 1),
                generatedLocation.Jaggedize(), generatedLocationVariants.Jaggedize())
            {
                SpawnPositionX = spawnRoom.Item1 * (RoomSizeX - 1) + RoomSizeX / 2,
                SpawnPositionY = spawnRoom.Item2 * (RoomSizeY - 1) + RoomSizeY / 2
            };

            foreach (var item in spawningQueue)
            {
                loc.SpawnEntity(item.Item2, item.Item1.Item1, item.Item1.Item2);
            }

            return loc;
        }

        private IEnumerable<EnumDirection> GetDoorDirections(int x, int y)
        {
            return new[]
            {
                generatedRooms[x - 1, y] != EnumRooms.NoRoom ? EnumDirection.Left : EnumDirection.Nowhere,
                generatedRooms[x, y - 1] != EnumRooms.NoRoom ? EnumDirection.Up : EnumDirection.Nowhere,
                generatedRooms[x + 1, y] != EnumRooms.NoRoom ? EnumDirection.Right : EnumDirection.Nowhere,
                generatedRooms[x, y + 1] != EnumRooms.NoRoom ? EnumDirection.Down : EnumDirection.Nowhere
            }.Where(dir => dir != EnumDirection.Nowhere).ToArray();
        }

        private void GenerateRoom(int roomX, int roomY)
        {
            if (generatedRooms[roomX, roomY] == EnumRooms.NoRoom) return;
            var absoluteX = roomX * (RoomSizeX - 1);
            var absoluteY = roomY * (RoomSizeY - 1);
            BuildRoomGeometry(roomX, roomY, absoluteX, absoluteY);

            foreach (var direction in GetDoorDirections(roomX, roomY))
            {
                int dx;
                int dy;
                GetDeltas(direction, out dx, out dy);
                generatedLocation[absoluteX + dx, absoluteY + dy] = TileAtlas.Instance.TileDoor.Id;
                generatedLocationVariants[absoluteX + dx, absoluteY + dy] = TileDoor.VariantDoorOpen;
            }

            if (random.Next(5) == 0 && generatedRooms[roomX, roomY] != EnumRooms.SpawnRoom)
            {
                PopulateRoomWithLava(absoluteX, absoluteY);
            }
            else
            {
                PopulateRoom(absoluteX, absoluteY);
            }
        }

        private void PopulateRoomWithLava(int absoluteX, int absoluteY)
        {
            for (int x = 1; x < RoomSizeX - 1; x++)
                for (int y = 1; y < RoomSizeY - 1; y++)
                    if (random.Next(5) == 0)
                    {
                        generatedLocationVariants[absoluteX + x + 1, absoluteY + y] =
                            (byte)((generatedLocationVariants[absoluteX + x + 1, absoluteY + y] % 16) +
                                    16 * (byte)ConsoleColor.DarkRed);
                        generatedLocationVariants[absoluteX + x - 1, absoluteY + y] =
                            (byte)((generatedLocationVariants[absoluteX + x - 1, absoluteY + y] % 16) +
                                    16 * (byte)ConsoleColor.DarkRed);
                        generatedLocationVariants[absoluteX + x, absoluteY + y - 1] =
                             (byte)((generatedLocationVariants[absoluteX + x, absoluteY + y - 1] % 16) +
                                    16 * (byte)ConsoleColor.DarkRed);
                        generatedLocationVariants[absoluteX + x, absoluteY + y + 1] =
                             (byte)((generatedLocationVariants[absoluteX + x - 1, absoluteY + y + 1] % 16) +
                                    16 * (byte)ConsoleColor.DarkRed);

                        generatedLocation[absoluteX + x, absoluteY + y] = TileAtlas.Instance.TileLava.Id;
                    }
        }

        private static void GetDeltas(EnumDirection direction, out int dx, out int dy)
        {
            dx = dy = 0;
            switch (direction)
            {
                case EnumDirection.Down:
                    dy = RoomSizeY - 1;
                    dx = RoomSizeX / 2;
                    return;
                case EnumDirection.Up:
                    dy = 0;
                    dx = RoomSizeX / 2;
                    return;
                case EnumDirection.Left:
                    dx = 0;
                    dy = RoomSizeY / 2;
                    return;
                case EnumDirection.Right:
                    dx = RoomSizeX - 1;
                    dy = RoomSizeY / 2;
                    return;
            }
        }

        private void PopulateRoom(int absoluteX, int absoluteY)
        {
            if (random.NextDouble() < MobChance)
            {
                spawningQueue.Add(new Tuple<Tuple<int, int>, Entity>(
                    new Tuple<int, int>(absoluteX + RoomSizeX / 2,
                        absoluteY + RoomSizeY / 2),
                    new EntityMonster()));
            }
        }

        private void BuildRoomGeometry(int roomX, int roomY, int absoluteX, int absoluteY)
        {
            for (var x = 0; x < RoomSizeX; x++)
            {
                for (var y = 0; y < RoomSizeY; y++)
                {
                    if (x == 0 || x == RoomSizeX - 1 || y == 0 || y == RoomSizeY - 1)
                    {
                        generatedLocation[absoluteX + x, absoluteY + y] = TileAtlas.Instance.TileWall.Id;

                        BuildSideWall(absoluteX, absoluteY, x, y);
                        BuildTopLeftCorner(roomX, roomY, absoluteX, absoluteY, x, y);
                        BuildTopRightCorner(roomX, roomY, absoluteX, absoluteY, x, y);
                        BuildBottomLeftCorner(roomX, roomY, absoluteX, absoluteY, x, y);
                        BuildBottomRightCorner(roomX, roomY, absoluteX, absoluteY, x, y);
                    }
                    else
                    {
                        generatedLocation[absoluteX + x, absoluteY + y] = TileAtlas.Instance.TileFloor.Id;
                        generatedLocationVariants[absoluteX + x, absoluteY + y] = (byte)(112 + random.Next(3));
                    }
                }
            }
        }

        private void BuildSideWall(int absoluteX, int absoluteY, int x, int y)
        {
            if (y == 0 || y == RoomSizeY - 1)
                generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 1;
            else generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 0;
        }

        private void BuildBottomRightCorner(int roomX, int roomY, int absoluteX, int absoluteY, int x, int y)
        {
            if (x == RoomSizeX - 1 && y == RoomSizeY - 1)
            {
                if (generatedRooms[roomX, roomY + 1] == EnumRooms.NoRoom)
                {
                    if (generatedRooms[roomX + 1, roomY] == EnumRooms.NoRoom)
                        generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 4;
                    else generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 9;
                }
                else
                {
                    generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 7;
                }

                var roomCount = 3;
                if (generatedRooms[roomX, roomY + 1] == EnumRooms.NoRoom) roomCount--;
                if (generatedRooms[roomX + 1, roomY] == EnumRooms.NoRoom) roomCount--;
                if (generatedRooms[roomX + 1, roomY + 1] == EnumRooms.NoRoom) roomCount--;

                if (roomCount == 2)
                {
                    generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 10;
                }
            }
        }

        private void BuildBottomLeftCorner(int roomX, int roomY, int absoluteX, int absoluteY, int x, int y)
        {
            if (x == 0 && y == RoomSizeY - 1)
            {
                if (generatedRooms[roomX, roomY + 1] == EnumRooms.NoRoom)
                {
                    if (generatedRooms[roomX - 1, roomY] == EnumRooms.NoRoom)
                        generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 5;
                    else generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 9;
                }
                else
                {
                    generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 6;
                }

                var roomCount = 3;
                if (generatedRooms[roomX, roomY + 1] == EnumRooms.NoRoom) roomCount--;
                if (generatedRooms[roomX - 1, roomY] == EnumRooms.NoRoom) roomCount--;
                if (generatedRooms[roomX - 1, roomY + 1] == EnumRooms.NoRoom) roomCount--;

                if (roomCount == 2)
                {
                    generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 10;
                }
            }
        }

        private void BuildTopLeftCorner(int roomX, int roomY, int absoluteX, int absoluteY, int x, int y)
        {
            if (x == 0 && y == 0)
            {
                if (generatedRooms[roomX, roomY - 1] == EnumRooms.NoRoom)
                {
                    if (generatedRooms[roomX - 1, roomY] == EnumRooms.NoRoom)
                        generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 2;
                    else generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 8;
                }
                else
                {
                    generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 6;
                }

                var roomCount = 3;
                if (generatedRooms[roomX, roomY - 1] == EnumRooms.NoRoom) roomCount--;
                if (generatedRooms[roomX - 1, roomY] == EnumRooms.NoRoom) roomCount--;
                if (generatedRooms[roomX - 1, roomY - 1] == EnumRooms.NoRoom) roomCount--;

                if (roomCount == 2)
                {
                    generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 10;
                }
            }
        }

        private void BuildTopRightCorner(int roomX, int roomY, int absoluteX, int absoluteY, int x, int y)
        {
            if (x == RoomSizeX - 1 && y == 0)
            {
                if (generatedRooms[roomX, roomY - 1] == EnumRooms.NoRoom)
                {
                    if (generatedRooms[roomX + 1, roomY] == EnumRooms.NoRoom)
                        generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 3;
                    else generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 8;
                }
                else
                {
                    generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 7;
                }

                var roomCount = 3;
                if (generatedRooms[roomX, roomY - 1] == EnumRooms.NoRoom) roomCount--;
                if (generatedRooms[roomX + 1, roomY] == EnumRooms.NoRoom) roomCount--;
                if (generatedRooms[roomX + 1, roomY - 1] == EnumRooms.NoRoom) roomCount--;

                if (roomCount == 2)
                {
                    generatedLocationVariants[absoluteX + x, absoluteY + y] = 112 + 10;
                }
            }
        }

        private void DetailLocation()
        {
            generatedLocation = new int[MaxRoomsX * (RoomSizeX - 1), MaxRoomsY * (RoomSizeY - 1)];
            generatedLocationVariants = new byte[MaxRoomsX * (RoomSizeX - 1), MaxRoomsY * (RoomSizeY - 1)];

            for (int x = 0; x < MaxRoomsX; x++)
            {
                for (int y = 0; y < MaxRoomsY; y++)
                {
                    GenerateRoom(x, y);
                }
            }
        }

        private void SetupRandomizer(int? seed)
        {
            random = seed == null ? new Random() : new Random((int)seed);
        }

        private void GenerateRoomMaze()
        {
            MazeGenerator.MaxRoomsX = MaxRoomsX;
            MazeGenerator.MaxRoomsY = MaxRoomsY;
            MazeGenerator.Random = random;
            generatedRooms = MazeGenerator.GenerateRooms(out spawnRoom);
        }
    }
}