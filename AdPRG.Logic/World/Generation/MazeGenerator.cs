﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdRPG.Logic.World.Generation
{
    public enum EnumRooms
    {
        NoRoom,
        ToBeDefined,
        SpawnRoom
    };

    class MazeGenerator
    {
        public static Random Random { get; set; }
        public static int MaxRoomsX { get; set; }
        public static int MaxRoomsY { get; set; }

        static MazeGenerator()
        {
            MaxRoomsX = 11;
            MaxRoomsY = 11;
            Random = new Random();
        }

        private static EnumRooms[,] FillBorders(EnumRooms[,] rooms)
        {
            for (var i = 0; i < MaxRoomsX; i++)
            {
                rooms[i, 0] = rooms[i, MaxRoomsY - 1] = EnumRooms.NoRoom;
            }

            for (var j = 0; j < MaxRoomsY; j++)
            {
                rooms[0, j] = rooms[MaxRoomsX - 1, j] = EnumRooms.NoRoom;
            }

            return rooms;
        }

        private static IEnumerable<Tuple<int, int>> GetAdjacentRooms(Tuple<int, int> point)
        {
            return new[] {
                new Tuple<int, int>(point.Item1 - 1, point.Item2),
                new Tuple<int, int>(point.Item1, point.Item2 - 1),
                new Tuple<int, int>(point.Item1 + 1, point.Item2),
                new Tuple<int, int>(point.Item1, point.Item2 + 1)
            }.Where(p => p.Item1 > 0 && p.Item1 < MaxRoomsX - 1 &&
                p.Item2 > 0 && p.Item2 < MaxRoomsY - 1).ToArray();
        }

        private static void MarkAdjacentWalls(Tuple<int, int> point, List<Tuple<int, int>> walls,
            EnumRooms[,] rooms)
        {
            foreach (var a in GetAdjacentRooms(point))
            {
                if (!walls.Any(w => w.Item1 == point.Item1 && w.Item2 == point.Item2)
                    && rooms[a.Item1, a.Item2] == EnumRooms.NoRoom)
                {
                    walls.Add(a);
                }
            }
        }

        private static Tuple<int, int> GetOnlyNeighboringRoom(EnumRooms[,] rooms, Tuple<int, int> wall)
        {
            var adjacentNonWalls = GetAdjacentRooms(wall)
                .Where(p => rooms[p.Item1, p.Item2] != EnumRooms.NoRoom)
                .ToList();

            if (adjacentNonWalls.Count == 1)
            {
                return adjacentNonWalls[0];
            }

            return null;
        }

        public static EnumRooms[,] GenerateRooms(out Tuple<int, int> startingPoint)
        {
            var rooms = new EnumRooms[MaxRoomsX, MaxRoomsY];
            var walls = new List<Tuple<int, int>>();
            startingPoint = new Tuple<int, int>
                ((Random.Next(MaxRoomsX - 2) * 2) / 2 + 1,
                (Random.Next(MaxRoomsY - 2) * 2) / 2 + 1);

            rooms[startingPoint.Item1, startingPoint.Item2] = EnumRooms.SpawnRoom;
            MarkAdjacentWalls(startingPoint, walls, rooms);

            var i = 0;
            while (walls.Any())
            {
                var randomWall = walls[Random.Next(walls.Count)];
                var onlyNeightbouringRoom = GetOnlyNeighboringRoom(rooms, randomWall);
                if (onlyNeightbouringRoom != null)
                {
                    var delta = new Tuple<int, int>(
                        randomWall.Item1 - onlyNeightbouringRoom.Item1,
                        randomWall.Item2 - onlyNeightbouringRoom.Item2);

                    var nextRoom = new Tuple<int, int>(
                        randomWall.Item1 + delta.Item1,
                        randomWall.Item2 + delta.Item2);

                    rooms[randomWall.Item1, randomWall.Item2] =
                        rooms[nextRoom.Item1, nextRoom.Item2] =
                            EnumRooms.ToBeDefined;

                    MarkAdjacentWalls(nextRoom, walls, rooms);
                }
                walls.Remove(randomWall);

                //DungeonGenerator.WriteMazeToFile(rooms, string.Format("D:/genDebug/step{0}.txt", i));
                i++;
            }

            return FillBorders(rooms);
        }

    }
}
