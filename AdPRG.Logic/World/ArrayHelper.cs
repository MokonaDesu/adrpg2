﻿using System.Collections.Generic;
using System.Linq;

namespace AdRPG.Logic.World
{
    public static class ArrayHelper
    {
        public static T[][] Jaggedize<T>(this T[,] input)
        {
            T[][] output = new T[input.GetLength(0)][];
            for (int i = 0; i < input.GetLength(0); i++)
            {
                output[i] = new T[input.GetLength(1)];
                for (int j = 0; j < input.GetLength(1); j++)
                {
                    output[i][j] = input[i, j];
                }
            }

            return output;
        }

        public static IEnumerable<IEnumerable<T>> GroupBy<T>(this IEnumerable<T> array, int size)
        {
            return array.Select((x, i) => new { Key = i / size, Value = x })
                                   .GroupBy(x => x.Key, x => x.Value);
        }
    }
}
