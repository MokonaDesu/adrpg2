﻿using AdRPG.Logic.Entities;
using AdRPG.Logic.World.Loc;
using System;

namespace AdRPG.Logic.AI
{
    [Serializable]
    class BehaviorRoam : IBehavior
    {
        private static readonly Random BehaviorRandomizer = new Random();
        
        private float continuationProbability;
        private const float ProbabilityDecay = 0.3f;

        private EnumDirection direction;

        public BehaviorRoam() {
            ShuffleBehavior();
        }

        public EntityActionResult Execute(EntityLiving entity)
        {
            if (TestForContinuation())
            {
                continuationProbability *= ProbabilityDecay;
            }
            else
            {
                ShuffleBehavior();
            }

            var result = entity.TryMove(direction);

            if (!result.Success)
            {
                continuationProbability = 0f;
                return Execute(entity);
            }

            return result;
        }

        private void ShuffleBehavior()
        {
            PickDirection();
            continuationProbability = 1f;
        }

        private bool TestForContinuation()
        {
            return BehaviorRandomizer.NextDouble() <= continuationProbability;
        }

        private void PickDirection()
        {
            direction = (EnumDirection)BehaviorRandomizer.Next(4);
        }    

    }
}
