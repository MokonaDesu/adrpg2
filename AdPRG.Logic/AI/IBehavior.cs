﻿using AdRPG.Logic.Entities;

namespace AdRPG.Logic.AI
{
    public interface IBehavior
    {
        EntityActionResult Execute(EntityLiving e);

    }
}
