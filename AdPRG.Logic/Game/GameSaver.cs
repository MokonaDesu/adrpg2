﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Json;
using AdRPG.Logic.Entities;
using AdRPG.Logic.World;
using AdRPG.Logic.World.Loc;

namespace AdRPG.Logic.Game
{
    class GameSaver
    {
        public static string LocationsFolder
        {
            get { return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "adrpg/saves"); }
        }

        public static IEnumerable<string> SavedLocations
        {
            get
            {
                var dirInfo = new DirectoryInfo(LocationsFolder);
                return dirInfo.EnumerateFiles("*.loc").Select(fd => fd.Name.Replace(".loc", string.Empty)).Distinct();
            }
        }

        private static void SaveGenericInformation(Location loc)
        {
            var serializer = new DataContractJsonSerializer(typeof(Location));
            var filePath = Path.Combine(LocationsFolder, loc.Name + ".loc");
            if (File.Exists(filePath))
                File.Delete(filePath);
            using (var fs = File.OpenWrite(filePath))
            {
                serializer.WriteObject(fs, loc);
            }
        }

        private static Location LoadGenericInformation(string locationName)
        {
            var serializer = new DataContractJsonSerializer(typeof(Location));
            using (var fs = File.OpenRead(Path.Combine(LocationsFolder, locationName + ".loc")))
            {
                return ((Location)serializer.ReadObject(fs)).PreInit();
            }
        }

        private static void LoadDetails(Location loc, out Player p)
        {
            var serializer = new BinaryFormatter();
            Metadata locationMetadata;
            using (var fs = File.OpenRead(Path.Combine(LocationsFolder, loc.Name + ".meta")))
            {
                locationMetadata = (Metadata)serializer.Deserialize(fs);
            }

            LoadEntities(loc, locationMetadata);
            p = LoadPlayer(locationMetadata.Read<Metadata>("player"));
            loc.SpawnEntity(p.Hero, p.Hero.X, p.Hero.Y);
        }

        private static void LoadEntities(Location loc, Metadata locationMetadata)
        {
            foreach (var item in locationMetadata.Read<Metadata>("entities").Items)
            {
                var entity = (Entity)Activator.CreateInstance(((Metadata)item).Read<Type>("type"));
                entity.Location = loc;
                entity.LoadFromMetadata((Metadata)item);
                loc.SpawnEntity(entity, entity.X, entity.Y);
            }
        }

        private static Player LoadPlayer(Metadata playerMetadata)
        {
            ObjectAtlas.UnregisterName(Game.Instance.CurrentPlayer.Hero);
            var entityPlayerMetadata = playerMetadata.Read<Metadata>("entityPlayer");
            var player = new Player()
            {
                Name = playerMetadata.Read<String>("playerName"),
            };
            var entityPlayer = new EntityPlayer(player);
            entityPlayer.LoadFromMetadata(entityPlayerMetadata);
            ObjectAtlas.RegisterName(entityPlayer, Game.Instance.CurrentPlayer.Name);
            player.Hero = entityPlayer;

            return player;
        }

        public static void Save(Location loc, Player p)
        {
            CreateGameFoldersIfNecessary();
            SaveGenericInformation(loc);
            SaveDetails(loc, p);
        }

        private static void SaveDetails(Location loc, Player p)
        {
            var formatter = new BinaryFormatter();
            var locationMetadata = new Metadata();
            locationMetadata.Write("entities", SaveEntitiesToMetadata(loc));
            locationMetadata.Write("player", SavePlayerToMetadata(loc, p));

            using (var fs = File.OpenWrite(Path.Combine(LocationsFolder, loc.Name + ".meta")))
            {
                formatter.Serialize(fs, locationMetadata);
            }
        }

        private static Metadata SavePlayerToMetadata(Location loc, Player p)
        {
            var playerMetadata = new Metadata();
            var entityMetadata = new Metadata();
            var entityPlayer = loc.Entities.FirstOrDefault(e => e is EntityPlayer);
            entityPlayer.SaveToMetadata(entityMetadata);
            playerMetadata.Write("entityPlayer", entityMetadata);
            playerMetadata.Write("playerName", p.Name);

            return playerMetadata;
        }

        private static Metadata SaveEntitiesToMetadata(Location loc)
        {
            var entityMetadata = new Metadata();
            foreach (var e in loc.Entities.Where(e => !(e is EntityPlayer)))
            {
                var m = new Metadata();
                e.SaveToMetadata(m);
                entityMetadata.Write(string.Format("{0}", e.Id.ToString()), m);
            }
            return entityMetadata;
        }

        private static void CreateGameFoldersIfNecessary()
        {
            var savesDirectory = new DirectoryInfo(LocationsFolder);
            if (!savesDirectory.Exists)
            {
                savesDirectory.Create();
            }
        }

        public static Location Load(string locationName, out Player p)
        {
            CreateGameFoldersIfNecessary();
            if (SavedLocations.Contains(locationName.Replace(".loc", string.Empty)))
            {
                Location location = LoadGenericInformation(locationName);
                LoadDetails(location, out p);
                return location;
            }
            else
            {
                throw new ArgumentException("No such location saved!");
            }
        }
    }
}
