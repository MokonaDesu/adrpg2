﻿using System;

namespace AdRPG.Logic.Game.ConsoleCommands
{
    class SaveCommand : ConsoleCommand
    {
        public override string CommandWord
        {
            get { return "save"; }
        }

        public override string Execute()
        {
            var game = Game.Instance;
            try
            {
                GameSaver.Save(game.CurrentLoc, game.CurrentPlayer);
                return string.Format("Saved {0}.", game.CurrentLoc.Name);
            }
            catch(Exception e)
            {
                return string.Format("{0} occured while saving :(", e.GetType().ToString());
            }      
        }
    }
}
