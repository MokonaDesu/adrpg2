﻿using AdRPG.Logic.Entities;
using System;

namespace AdRPG.Logic.Game.ConsoleCommands
{
    class SpawnMonsterCommand : ConsoleCommand
    {
        public override string CommandWord
        {
            get { return "spawnmonster"; }
        }

        public override string Execute()
        {
            var game = Game.Instance;
            try
            {
                Game.Instance.CurrentLoc.SpawnEntity(new EntityMonster(), 
                    Game.Instance.CurrentLoc.SpawnPositionX,
                    Game.Instance.CurrentLoc.SpawnPositionY);

                return string.Format("Spawned monster.");
            }
            catch(Exception e)
            {
                return string.Format("{0} occured while spawning :(", e.GetType().ToString());
            }      
        }
    }
}
