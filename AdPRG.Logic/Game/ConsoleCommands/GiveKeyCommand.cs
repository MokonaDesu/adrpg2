﻿using AdRPG.Logic.Items;
using AdRPG.Logic.World.Tiles;

namespace AdRPG.Logic.Game.ConsoleCommands
{
    class GiveKeyCommand : ConsoleCommand
    {
        public override string CommandWord
        {
            get { return "givekey"; }
        }

        public override string Execute()
        {
            Game.Instance.CurrentPlayer.Hero.Items.Add(new ItemKey());
            return string.Format("Gave 1 key to {0}", ObjectAtlas.NameFor(Game.Instance.CurrentPlayer));
        }
    }
}
