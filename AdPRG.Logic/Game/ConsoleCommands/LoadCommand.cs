﻿using AdRPG.Logic.World.Loc;
using System;

namespace AdRPG.Logic.Game.ConsoleCommands
{
    class LoadCommand : ConsoleCommand
    {
        public override string CommandWord
        {
            get { return "load"; }
        }

        public override string Execute()
        {
            var game = Game.Instance;
            try
            {
                Player player;
                Location location = GameSaver.Load(game.CurrentLoc.Name, out player);

                Game.Instance.CurrentLoc = location;
                Game.Instance.CurrentPlayer = player;

                return string.Format("Loaded {0}.", game.CurrentLoc.Name);
            }
            catch(Exception e)
            {
                return string.Format("{0} occured while loading :(", e.GetType().ToString());
            }      
        }
    }
}
