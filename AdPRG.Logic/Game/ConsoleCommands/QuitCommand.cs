﻿namespace AdRPG.Logic.Game.ConsoleCommands
{
    class QuitCommand : ConsoleCommand
    {
        public override string CommandWord
        {
            get { return "quit"; }
        }

        public override string Execute()
        {
            Game.Instance.Shutdown();
            return  "Bye...";
        }
    }
}
