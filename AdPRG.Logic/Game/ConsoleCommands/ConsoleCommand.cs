﻿using System.Collections.Generic;
using System.Linq;

namespace AdRPG.Logic.Game.ConsoleCommands
{
    public abstract class ConsoleCommand
    {
        public static List<ConsoleCommand> Commands { get; private set; }

        static ConsoleCommand()
        {
            Commands = new List<ConsoleCommand>
            {
                new SaveCommand(),
                new LoadCommand(),
                new SpawnMonsterCommand(),
                new QuitCommand(),
                new GiveKeyCommand()
            };
        }

        public static string ExecuteCommandByName(string name)
        {
            var command = Commands.FirstOrDefault(c => c.CommandWord.ToLowerInvariant().Equals(name.ToLowerInvariant()));
            if (command != null)
            {
                return command.Execute();
            }

            return string.Format("I don't know how to '{0}!'", name);
        }

        public abstract string CommandWord { get; }

        public abstract string Execute();
    }
}
