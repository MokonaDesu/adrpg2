﻿using System.Collections.Generic;

namespace AdRPG.Logic.Game
{
    public class ObjectAtlas
    {
        private static readonly Dictionary<object, string> Names = new Dictionary<object, string>();

        public static void RegisterName(object item, string name)
        {
            Names[item] = name;
        }

        public static void UnregisterName(object item)
        {
            if (Names.ContainsKey(item))
                Names.Remove(item);
        }

        public static string NameFor(object item)
        {
            if (Names.ContainsKey(item))
            {
                return Names[item];
            }

            if (Names.ContainsKey(item.GetType()))
            {
                return Names[item.GetType()];
            }

            return item.GetType().Name;
        }
    }
}
