﻿using System.Collections.Generic;
using AdRPG.Logic.Entities;

namespace AdRPG.Logic.Game
{
    public class InteractableTileAdapter : IInteractable
    {
        private readonly IInteractableTile tileData;

        public InteractableTileAdapter(IInteractableTile tile, int x, int y)
        {
            tileData = tile;
            X = x;
            Y = y;
        }

        public IList<EnumInteraction> GetPossibleInteractions(EntityLiving e)
        {
            return tileData.GetPossibleInteractions(X, Y, e, Game.Instance.CurrentLoc.GetTileVariant(X, Y));
        }

        public EntityActionResult OnInteract(EntityLiving e, EnumInteraction interactionCode)
        {
            return tileData.OnInteract(X, Y, e, interactionCode, Game.Instance.CurrentLoc.GetTileVariant(X, Y));
        }

        public int X { get; private set; }
        public int Y { get; private set; }
    }
}
