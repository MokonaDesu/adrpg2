﻿using System.Collections.Generic;
using AdRPG.Logic.Entities;
using AdRPG.Logic.World;

namespace AdRPG.Logic.Game
{
    public enum EnumInteraction
    {
        PickUp,
        Attack,
        Open,
        Close,
        Break,
        Inspect
    }

    public interface IInteractable
    {
        IList<EnumInteraction> GetPossibleInteractions(EntityLiving e);

        EntityActionResult OnInteract(EntityLiving e, EnumInteraction interactionCode);

        int X { get; }

        int Y { get; }
    }

    public interface IInteractableTile
    {
        IList<EnumInteraction> GetPossibleInteractions(int x, int y, EntityLiving e, byte variant);

        EntityActionResult OnInteract(int x, int y, EntityLiving e, EnumInteraction interactionCode, byte variant);

    }
}
