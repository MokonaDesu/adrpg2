﻿using System.Collections.Generic;
using AdRPG.Logic.Entities;
using AdRPG.Logic.World.Generation;
using AdRPG.Logic.World.Loc;

namespace AdRPG.Logic.Game
{
    public class Game
    {
        #region [ Static Data ]

        #endregion

        #region [ Singleton ]
        private static Game instance;

        public static Game Instance
        {
            get
            {
                return instance ?? (instance = new Game());
            }
        }

        #endregion

        public Location CurrentLoc { get; set; }
        public Player CurrentPlayer { get; set; }

        public bool Exiting { get; private set; }

        private Game()
        {
            SetupGenerators();
            SetupPlayer();
            SetupObjectAtlas();
            GenerateLoc();
        }

        private void SetupObjectAtlas()
        {
            ObjectAtlas.RegisterName(CurrentPlayer.Hero, CurrentPlayer.Name);
            ObjectAtlas.RegisterName(typeof(EntityMonster), "Monster");
        }

        private void SetupGenerators()
        {
            LocationGeneratorFactory.Instance.RegisterGenerator("dungeon", new DungeonGenerator());
        }

        private void GenerateLoc()
        {
            ILocationGenerator generator = LocationGeneratorFactory.Instance.Generator("dungeon");
            CurrentLoc = generator.Generate(null);
            CurrentLoc.SpawnEntity(CurrentPlayer.Hero, CurrentLoc.SpawnPositionX, CurrentLoc.SpawnPositionY);
        }

        private void SetupPlayer()
        {
            var player = new Player();
            player.Hero = new EntityPlayer(player);
            player.Name = "Developer";

            CurrentPlayer = player;
        }

        public void Update(List<string> results)
        {
            CurrentLoc.UpdateEntityList();

            foreach (var entity in CurrentLoc.Entities)
            {
                if (entity is EntityMonster)
                {
                    results.AddRange((entity as EntityMonster).Behavior.Execute(entity as EntityLiving).Messages);
                }
            }
        }

        public void Shutdown()
        {
            Exiting = true;
        }
    }
}
