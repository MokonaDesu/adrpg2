﻿using System.Collections.Generic;
using AdRPG.Logic.Game;
using AdRPG.Logic.World;
using AdRPG.Logic.World.Loc;
using System;

namespace AdRPG.Logic.Entities
{
    public abstract class Entity : IInteractable
    {
        public Location Location { get; set; }

        #region [ Positioning ]
        private int x;
        public int X
        {
            get { return x; }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Location coordinate value can't be negative");

                x = value;
            }
        }
        private int y;
        public int Y
        {
            get { return y; }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Location coordinate value can't be negative");

                y = value;
            }
        }

        public EnumDirection Facing
        {
            get;
            set;
        }
        #endregion

        #region [ Abstract Properties ]

        public virtual char GetDrawing()
        {
            return 'E';
        }

        public virtual ConsoleColor GetBackground()
        {
            return ConsoleColor.Black;
        }

        public virtual ConsoleColor GetForeground()
        {
            return ConsoleColor.White;
        }

        #endregion

        protected Entity()
        {
            Id = Guid.NewGuid();
        }

        #region [ Saving ]

        public Guid Id { get; private set; }

        public virtual void SaveToMetadata(Metadata m)
        {
            m.Write("type", GetType());
            m.Write("id", Id);
            m.Write("facing", Facing);
            m.Write("position_x", X);
            m.Write("position_y", Y);
        }

        public virtual void LoadFromMetadata(Metadata m)
        {
            Id = m.Read<Guid>("id");
            Facing = m.Read<EnumDirection>("facing");
            X = m.Read<int>("position_x");
            Y = m.Read<int>("position_y");
        }

        #endregion

        #region [ IInteractable ]
        public virtual IList<EnumInteraction> GetPossibleInteractions(EntityLiving e)
        {
            return new List<EnumInteraction>();
        }

        public virtual EntityActionResult OnInteract(EntityLiving e, EnumInteraction interactionCode)
        {
            var result = new EntityActionResult();
            return result;
        }
        #endregion

        public override string ToString()
        {
            return ObjectAtlas.NameFor(this);
        }
    }
}
