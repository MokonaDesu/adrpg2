﻿using AdRPG.Logic.Entities;
using AdRPG.Logic.World.Loc;
using AdRPG.Logic.World.Tiles;

using System;
using System.Linq;

namespace AdRPG.Logic.Entities
{
    static class EntityActionHelper
    {
        public static void TryMove(this Entity entity, EnumDirection direction, out bool result)
        {
            int offsetX = direction.OffsetX();
            int offsetY = direction.OffsetY();

            result = TileAtlas.Instance.GetById(entity.Location.TileId(entity.X + offsetX, entity.Y + offsetY)).CanPass(new Metadata());
            
            if (result) {
                entity.Facing = direction;

                var attackTarget = entity.Location.Entities.Where(e => e.X == entity.X + offsetX && e.Y == entity.Y + offsetY).FirstOrDefault();
                if (attackTarget != null && attackTarget != entity)
                {
                    entity.TryAttack(attackTarget, out result);
                    return;
                }

                entity.X += offsetX;
                entity.Y += offsetY;
            }

            if (entity is EntityPlayer)
            {
                (entity as EntityPlayer).Player.Console.AddMessage(string.Format("Trying to move {0}. Result: {1}.", direction, result ? "success" : "fail"));
            }
        }

        public static void TryAttack(this Entity entity, Entity target, out bool result)
        {
            double distance = Math.Pow(entity.X - target.X, 2) + Math.Pow(entity.Y - target.Y, 2);
            float backstabMultiplyer = 1.0f;

            //Adjacent tiles
            if (distance < 4)
            {
                int damage = 5;

                if (entity.Facing == target.Facing)
                    backstabMultiplyer = 2f;
                
                target.Damage((int)(damage * backstabMultiplyer));
                result = true;
            }
            else
            {
                result = false;
            }

            if (entity is EntityPlayer)
            {
                (entity as EntityPlayer).Player.Console.AddMessage(string.Format("Trying to hit {0}. Result: {1}.",
                    target,
                    result ? (backstabMultiplyer > 1f ? "backstab" : "hit") : "miss"));
            }

            if (target is EntityPlayer)
            {
                (target as EntityPlayer).Player.Console.AddMessage(string.Format("Hit by {0}. Result: {1}.",
                    entity,
                    result ? (backstabMultiplyer > 1f ? "backstab" : "hit") : "miss"));
            }

        }

    }
}
