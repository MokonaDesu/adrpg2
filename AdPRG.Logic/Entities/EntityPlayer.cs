﻿using AdRPG.Logic.Game;
using AdRPG.Logic.World;

namespace AdRPG.Logic.Entities
{
    public class EntityPlayer : EntityLiving
    {
        public Player Player { get; protected set; }

        public int VisionRadius { get; protected set; }

        public EntityPlayer(Player player)
        {
            BaseHealth = 200;
            Health = MaxHealth;
            VisionRadius = 10;
            Player = player;
        }

        public override char GetDrawing()
        {
            return 'P';
        }

        public override void SaveToMetadata(Metadata m)
        {
            base.SaveToMetadata(m);
            m.Write("visionRadius", VisionRadius);
        }

        public override void LoadFromMetadata(Metadata m)
        {
            base.LoadFromMetadata(m);
            VisionRadius = m.Read<int>("visionRadius");
        }
    }
}
