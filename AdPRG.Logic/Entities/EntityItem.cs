﻿using System;
using System.Collections.Generic;
using AdRPG.Logic.Game;
using AdRPG.Logic.Items;
using AdRPG.Logic.World;

namespace AdRPG.Logic.Entities
{
    class EntityItem : Entity
    {
        private Item Item { get; set; }

        public EntityItem(Item item)
        {
            Item = item;
        }

        public override char GetDrawing()
        {
            return 'i';
        }

        public override ConsoleColor GetForeground()
        {
            return ConsoleColor.Red;
        }

        public override void SaveToMetadata(Metadata m)
        {
            base.SaveToMetadata(m);

            //m.Write("item data", 3);
        }

        public override void LoadFromMetadata(Metadata m)
        {
            base.LoadFromMetadata(m);

           // Item = new ItemStack().LoadFromMetadata(m);
        }

        #region [ IInteractable ]
        public IList<EnumInteraction> GetPossibleInteractions(EntityLiving e, Metadata m, byte variant)
        {
            var distance = Math.Sqrt(Math.Pow(e.X - X, 2) + Math.Pow(e.Y - Y, 2));

            return distance <= GameRules.MaxInterationDistance ? new List<EnumInteraction> { EnumInteraction.PickUp } : new List<EnumInteraction>();
        }

        public EntityActionResult OnInteract(EntityLiving e, Metadata m, EnumInteraction interactionCode, byte variant)
        {
            return new EntityActionResult() {Success = true}.AddMessage("Interacted with item!");
        }
        #endregion
    }
}
