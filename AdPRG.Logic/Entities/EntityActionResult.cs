﻿using System.Collections.Generic;

namespace AdRPG.Logic.Entities
{
    public class EntityActionResult
    {
        public List<string> Messages { get; set; }

        public bool Success { get; set; }

        public EntityActionResult()
        {
            Messages = new List<string>();
        }

        public EntityActionResult AddMessage(string s)
        {
            Messages.Add(s);
            return this;
        }
    }
}
