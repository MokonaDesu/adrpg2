﻿using System;
using AdRPG.Logic.AI;

namespace AdRPG.Logic.Entities
{
    class EntityMonster : EntityLiving
    {
        public IBehavior Behavior
        {
            get;
            protected set;
        }
        
        public EntityMonster()
        {
            BaseHealth = 7;
            Behavior = new BehaviorRoam();
        }

        public override char GetDrawing()
        {
            return 'M';
        }

        public override ConsoleColor GetForeground()
        {
            return ConsoleColor.Red;
        }
    }
}
