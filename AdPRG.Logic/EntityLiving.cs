﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AdRPG.Logic.Entities;
using AdRPG.Logic.Game;
using AdRPG.Logic.Items;
using AdRPG.Logic.World;
using AdRPG.Logic.World.Loc;
using AdRPG.Logic.World.Tiles;

namespace AdRPG.Logic
{
    public class EntityLiving : Entity, IInventory
    {
        #region [ S.P.E.C.I.A.L. ]

        public int Strength { get; set; }

        public int Perception { get; set; }

        public int Endurance { get; set; }

        public int Charisma { get; set; }

        public int Intellegence { get; set; }

        public int Agility { get; set; }

        public int Luck { get; set; }

        #endregion

        #region [ Health ]

        public int BaseHealth { get; protected set; }

        private int health;

        public int Health
        {
            get { return health; }
            set { health = Math.Max(0, Math.Min(value, MaxHealth)); }
        }

        public int MaxHealth
        {
            get { return (int)Math.Round(BaseHealth + GameRules.EnduranceHealthModifier * Math.Log(Endurance)); }
        }

        public bool IsDead
        {
            get { return Health <= 0; }
        }

        public void Kill()
        {
            Health = 0;
        }

        public void Damage(int strength)
        {
            Health -= strength;
        }

        #endregion

        #region [ Saving ]

        public override void SaveToMetadata(Metadata m)
        {
            base.SaveToMetadata(m);

            m.Write("baseHealth", BaseHealth);

            m.Write("strength", Strength);
            m.Write("perception", Perception);
            m.Write("endurance", Endurance);
            m.Write("charisma", Charisma);
            m.Write("intellegence", Intellegence);
            m.Write("agility", Agility);
            m.Write("luck", Luck);
        }

        public override void LoadFromMetadata(Metadata m)
        {
            base.LoadFromMetadata(m);

            BaseHealth = m.Read<int>("baseHealth");

            Strength = m.Read<int>("strength");
            Perception = m.Read<int>("perception");
            Endurance = m.Read<int>("endurance");
            Charisma = m.Read<int>("charisma");
            Intellegence = m.Read<int>("intellegence");
            Agility = m.Read<int>("agility");
            Luck = m.Read<int>("luck");
        }

        #endregion

        #region [ Movement ]

        public EntityActionResult TryMove(EnumDirection direction)
        {
            var targetTile = TileAtlas.Instance.GetById(Location.TileId(X + direction.OffsetX(), Y + direction.OffsetY()));
            var targetTileVariant = Location.GetTileVariant(X + direction.OffsetX(), Y + direction.OffsetY());

            if (targetTile.CanPass(this, targetTileVariant))
            {
                var targetEntity =
                    Game.Game.Instance.CurrentLoc.Entities.FirstOrDefault(
                        e => e.X == X + direction.OffsetX() && e.Y == Y + direction.OffsetY());

                if (targetEntity == null)
                {
                    X += direction.OffsetX();
                    Y += direction.OffsetY();
                    return new EntityActionResult { Success = true };
                }
                else
                {
                    var interactions = targetEntity.GetPossibleInteractions(this);

                    if (interactions.Any())
                    {
                        return targetEntity.OnInteract(this, interactions.First());
                    }

                    return new EntityActionResult { Success = false };
                }
            }
            
            var interactableTile = targetTile as IInteractableTile;
            if (interactableTile != null)
            {
                var interactions = interactableTile.GetPossibleInteractions(X + direction.OffsetX(),
                    Y + direction.OffsetY(), this, targetTileVariant);

                if (interactions.Any())
                {
                    return interactableTile.OnInteract(X + direction.OffsetX(), Y + direction.OffsetY(), this,
                        interactions.First(), targetTileVariant);
                }
            }

            return new EntityActionResult { Success = false };
        }

        #endregion

        public EntityLiving()
        {
            Strength =
            Perception =
            Endurance =
            Charisma =
            Intellegence =
            Agility =
            Luck = 10;

            BaseHealth = 10;
            Health = MaxHealth;
        }

        public override IList<EnumInteraction> GetPossibleInteractions(EntityLiving e)
        {
            var interactions = new List<EnumInteraction>();

            if (CanBeAttacked(e))
            {
                interactions.Add(EnumInteraction.Attack);

            }

            if (CanBeInspected(e))
            {
                interactions.Add(EnumInteraction.Inspect);
            }

            return interactions;
        }

        public override EntityActionResult OnInteract(EntityLiving e, EnumInteraction interactionCode)
        {
            switch (interactionCode)
            {
                case EnumInteraction.Attack:
                    return OnEntityAttacked(e);
                case EnumInteraction.Inspect:
                    return OnEntityInspected(e);
            }

            return new EntityActionResult();
        }

        private EntityActionResult OnEntityInspected(EntityLiving entityLiving)
        {
            var builder = new StringBuilder();
            builder.AppendFormat("This is {0}. ", ToString());
            builder.Append("Dark creature that roams aimlessly halls of the Test Dungeon... ");
            builder.Append("Its blind eyes are deep black, its rotting skin is dark red. ");
            builder.Append("Seems that this creature can't see, so it won't attack unless it runs directly into someone.");

            return new EntityActionResult {Success = false}.AddMessage(builder.ToString());
        }

        private EntityActionResult OnEntityAttacked(EntityLiving attacker)
        {
            if (IsDead) return new EntityActionResult { Success = true };

            double distance = Math.Sqrt(Math.Pow(X - attacker.X, 2) + Math.Pow(Y - attacker.Y, 2));
            bool result;
            float backstabMultiplyer = 1f;
            int damage = 0;

            //Adjacent tiles
            if (distance < GameRules.MeleeAttackRange)
            {
                damage = 5;

                if (attacker.Facing == Facing)
                    backstabMultiplyer = GameRules.BackstabMultiplyer;

                this.Damage((int)(damage * backstabMultiplyer));
                result = true;
            }
            else
            {
                result = false;
            }

            var actionResult = new EntityActionResult { Success = result };

            if (this is EntityPlayer || attacker is EntityPlayer)
            {
                AddActionMessage(attacker, this, actionResult, result, damage, backstabMultiplyer);
            }

            return actionResult;
        }

        private static void AddActionMessage(EntityLiving entity, EntityLiving target, EntityActionResult actionResult,
            bool result, int damage, float backstabMultiplyer)
        {
            actionResult.Messages.Add(BuildAttackMessage(entity, target, result, damage, backstabMultiplyer));

            if (target.IsDead)
            {
                actionResult.Messages.Add(BuildDeathMessage(target));
            }
        }

        private static string BuildDeathMessage(EntityLiving target)
        {
            return string.Format(("{0} dies!"), ObjectAtlas.NameFor(target));
        }

        private static string BuildAttackMessage(EntityLiving entity, EntityLiving target, bool result, int damage, float backstabMultiplyer)
        {
            return string.Format(("{0} attacks {1}: {2}{3} ({4}/{5})."),
                    ObjectAtlas.NameFor(entity),
                    ObjectAtlas.NameFor(target),
                    result ? (backstabMultiplyer > 1f ? "backstab" : "hit") : "miss",
                    result ? " for " + damage : "",
                    target.Health,
                    target.MaxHealth);
        }

        private bool CanBeInspected(EntityLiving e)
        {
            return Math.Sqrt(Math.Pow(e.X - X, 2) + Math.Pow(e.Y - Y, 2)) <=
                   Game.Game.Instance.CurrentPlayer.Hero.VisionRadius;
        }

        private bool CanBeAttacked(EntityLiving e)
        {
            return Math.Sqrt(Math.Pow(e.X - X, 2) + Math.Pow(e.Y - Y, 2)) <= GameRules.MeleeAttackRange;
        }

        #region [ Inventory ]

        private readonly List<Item> items = new List<Item>();

        public Item GetItemInSlot(int slot)
        {
            return items[slot];
        }

        public void SetItemInSlot(int slot, Item item)
        {
            items[slot] = item;
        }

        public IList<Item> Items { get { return items; } }

        #endregion
    }
}
