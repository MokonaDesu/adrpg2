﻿using System.Collections.Generic;

namespace AdRPG.Logic.Items
{
    public interface IInventory
    {
        IList<Item> Items { get; } 
    }
}
