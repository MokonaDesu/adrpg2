﻿namespace AdRPG.Logic
{
    public static class GameRules
    {
        public static float EnduranceHealthModifier = 3f;
        public static float BackstabMultiplyer = 2f;
        public static float MaxInterationDistance = 3f;
        public static float MeleeAttackRange = 1.5f;
    }
}